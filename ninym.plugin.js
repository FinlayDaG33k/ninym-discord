/**
  * @name Ninym
  * @source https://gitlab.com/FinlayDaG33k/ninym-discord
  * @description Integrates Ninym into Discord for automatic status chances
  * @version 0.2.0
  */
 module.exports = class Ninym {
  authToken = '';
  initialized = false;
  availability = {
    isUpdating: false,
    interval: null,
    statusMap: {
      'available': {
        text: 'Ship goes HONK HONK!',
        status: 'dnd',
        emojiName: '🚢'
      },
      'eating': {
        text: 'Eating.',
        status: 'idle',
        emojiName: '🍽'
      },
      'busy': {
        text: 'Busy strategizing.',
        status: 'dnd',
        emojiName: '🖥️'
      },
      'chilling': {
        text: 'Conducting research of Anime.',
        status: 'idle',
        emojiName: '🐢'
      },
      'brb': {
        text: 'Back in a bit.',
        status: 'dnd',
        emojiName: '⌛'
      },
      'nap': {
        text: 'Taking a nap.',
        status: 'dnd',
        emojiName: '💤'
      },
      'afk': {
        text: 'Out for sortie.',
        status: 'idle',
        emojiName: '⚓'
      }
    }
  };
  statusCache = {
    status: null,
    custom_status: {
      text: '',
      expires_at: null,
      emoji_id: null,
      emoji_name: null
    }
  };

  constructor() {}

  // Meta
  getName() { return "Ninym"; }
  getShortName() { return "Ninym"; }
  getDescription() { return "Discord integration for Ninym"; }
  getVersion() { return "0.1.0"; }
  getAuthor() { return "FinlayDaG33k"; }
    
  // Load/Unload
  load() {}
  unload() {}

  // Events
  onMessage(message) {};
  onSwitch() {};
  observer(e) {};
    
  // Start/Stop
  async start() {
    // Obtain our authToken
    this.authToken = this.getToken();

    // Obtain our current status
    await this.getStatus();

    // Start interval
    this.createInterval();

    // Show toast that we've started
    window.BdApi.showToast(`${this.getName()} ${this.getVersion()} has started`);
  }

  stop() {
    clearInterval(this.availability.interval);
    window.BdApi.showToast(`${this.getName()} ${this.getVersion()} has stopped.`);
  };

  createInterval() {
    this.availability.interval = setInterval(async () => {
      // Make sure we aren't updating already
      if(this.availability.isUpdating === true) return;

      // Run an update
      this.availability.isUpdating = true;
      try {
        await this.updateAvailability();
      } catch(e) {
        console.error(`[NINYM] Could not update availability: ${e.message}`);
      }
      
      // Mark as finished
      this.availability.isUpdating = false;
    }, 5000);
  }

  async updateAvailability() {
    let resp;
    try {
      resp = await fetch('https://ninym.finlaydag33k.nl/availability');
    } catch(e) {
      console.warn(`[NINYM] Could not obtain status: ${e.message}`);
      return;
    }
    
    // Check if the response was OK
    if(!resp.ok) return;

    // Get the availability
    const newAvailability = await resp.text();

    // Change text if need be
    if(this.statusCache.status !== this.availability.statusMap[newAvailability].status || this.statusCache.custom_status.text !== this.availability.statusMap[newAvailability].text) {
      const newStatus = {
        status: this.availability.statusMap[newAvailability].status,
        custom_status: {
          text: this.availability.statusMap[newAvailability].text,
          expires_at: null,
          emoji_id: null,
          emoji_name: this.availability.statusMap[newAvailability].emojiName
        }
      };
      await this.setStatus(newStatus);
      this.statusCache = newStatus;
    }
  }

  async getStatus() {
    try {
      const resp = await fetch('https://discord.com/api/v9/users/@me/settings', {
        method: 'GET',
        headers: {
          authorization: this.authToken,
        },
      });
      if(!resp.ok) throw Error(`Got non-OK status: ${resp.statusText}`);

      // Parse JSON
      const data = await resp.json();

      // Cache our status
      this.statusCache = {
        status: data.status,
        custom_status: data.custom_status,
      };
    } catch(e) {
      console.error(`[NINYM] Could not get status: "${e.message}"`);
    }
  }

  async setStatus(status) {
    try {
      const resp = await fetch('https://discord.com/api/v9/users/@me/settings', {
        method: 'PATCH',
        headers: {
          authorization: this.authToken,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(status),
      });
      if(!resp.ok) throw Error(`Got non-OK status: ${resp.statusText}`);
    } catch (e) {
      console.error(`[NINYM] Could not set status: "${e.message}"`);
    }
  }

  /**
   * Get authtoken from *somewhere* (idh, this is magic to me)
   * 
   * TODO: Fix issue where this function can only be ran once
   */
  getToken() {
    let a = [];
    webpackChunkdiscord_app.push([[0],,e=>Object.keys(e.c).find(t=>(t=e(t)?.default?.getToken?.())&&a.push(t))]);
    return a[0];
  }
}
